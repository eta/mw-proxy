;; (ql:quickload '(drakma hunchentoot babel cl-json))
(defpackage :mw-proxy
  (:use :cl))

(in-package :mw-proxy)

(defparameter *etawiki-base-url* "https://wiki.i.eta.st/api.php")

(defvar *cookie-jar* (make-instance 'drakma:cookie-jar))
(defvar *auth-details* nil
  "Stored authentication details, in order to reauthenticate if the session expires.")

(defun aval (key object)
  (cdr (assoc key object)))

(defun aval-path (object &rest path)
  (let ((ptr object))
    (loop
      for thing in path
      do (setf ptr (aval thing ptr)))
    ptr))

(defun mw-request (parameters &key (method :get))
  "Make a request to the MediaWiki API, and decode and return the result."
  (when (null (assoc "format" parameters :test #'string=))
    (setf parameters (append '(("format" . "json")) parameters)))
  (multiple-value-bind (body status-code)
      (drakma:http-request *etawiki-base-url*
                           :force-binary t
                           :method method
                           :cookie-jar *cookie-jar*
                           :parameters parameters)
    (let ((body (babel:octets-to-string body :encoding :utf-8)))
      (unless (eql status-code 200)
        (error "etawiki returned code ~A:~%~2@T~A" status-code body))
      (let* ((ret (cl-json:decode-json-from-string body))
             (maybe-err (cdr (assoc :error ret))))
        (when maybe-err
          (error "wiki returned error code ~A:~%~2@T~A"
                 (cdr (assoc :code maybe-err))
                 (cdr (assoc :info maybe-err))))
        ret))))

(defun mw-reset ()
  "Clear the MediaWiki cookie jar."
  (setf *cookie-jar* (make-instance 'drakma:cookie-jar)))

(defun mw-userinfo ()
  "Get information about the currently logged in user. Typically returns an error if not logged in."
  (mw-request `(("action" . "query")
                ("meta" . "userinfo"))))

(define-condition empty-token-error (error)
  ((type :initarg :type :reader token-type))
  (:report (lambda (c s)
             (format s "Got empty token of type ~A" (token-type c)))))

(defun retrieve-tokens (&optional (type :csrf))
  "Retrieve a MediaWiki API token for the given TYPE of action."
  (check-type type (member :csrf :createaccount :deleteglobalaccount :login
                           :patrol :rollback :setglobalaccountstatus
                                 :userrights :watch))
  (restart-case
      (let* ((result
               (mw-request `(("action" . "query")
                             ("meta" . "tokens")
                             ("type" .
                                     ,(string-downcase (symbol-name type))))))
             (token-alist (aval-path result :query :tokens)))
        (unless (eql (length token-alist) 1)
          (error "Unexpected token response: ~A" result))
        (let ((ret (cdr (first token-alist))))
          (when (or (not ret) (string= ret "+\\"))
            (error 'empty-token-error
                   :type type))
          ret))
    (reauthenticate ()
      :report "Attempt to reauthenticate with stored credentials, and retry."
      :test (lambda (c)
              (and
               (typep c 'empty-token-error)
               *auth-details*
               (not (eql type :login))))
      (mw-login (car *auth-details*) (cdr *auth-details*))
      (retrieve-tokens type))))

(defun mw-login (username botpassword &key storep)
  "Log in to MediaWiki with a username and a bot password from Special:BotPaswwords. Returns the username we logged in as.
If STOREP is true, store the credentials in case of successful auth, to be used with the REAUTHENTICATE restart."
  (let* ((token (retrieve-tokens :login))
         (object
           (mw-request `(("lgname" . ,username)
                         ("action" . "login")
                         ("lgtoken" . ,token)
                         ("lgpassword" . ,botpassword))
                       :method :post))
         (login-result (aval :login object)))
    (unless (string= (aval :result login-result) "Success")
      (error "Login returned ~A: ~A"
             (aval :result login-result)
             (aval :reason login-result)))
    (when storep
      (setf *auth-details* (cons username botpassword)))
    (aval :lgusername login-result)))

(defun mw-create (page-title content)
  "Create a new MediaWiki page under the provided PAGE-TITLE, using the provided CONTENT."
  (let* ((token (retrieve-tokens))
         (result
           (mw-request `(("action" . "edit")
                         ("title" . ,page-title)
                         ("text" . ,content)
                         ;; note: the value doesn't matter, this is true
                         ;; if provided and false if not
                         ("createonly" . "true")
                         ;; same
                         ("bot" . "true")
                         ("token" . ,token))
                       :method :post)))
    result))

(defun start-webserver ()
  (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor
                                    :port 4000
                                    :address "::"
                                    ;; FIXME(eta): error handling
                                    ;;:error-template-directory nil
                                    )))
